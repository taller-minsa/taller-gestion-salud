# Establecimiento de salud

En el archivo `models/gestion.py`


```python


class Establecimiento(models.Model):
    _name = 'gestion.establecimiento'
    _description = u'Establecimiento de salud'

    _inherits = {'res.company': 'company_id'}

    ipress = fields.Char(
        'Código Ipress',
        required=True,
        size=6,
    )
```

En el archivo `views/gestion_views.xml`

```xml
                        
        <!-- VISTAS DEL MODELO ESTABLECIMIENTO -->
        
        <!-- VISTA SEARCH -->
        <record id="establecimiento_search_view" model="ir.ui.view">
            <field name="name">gestion.establecimiento.search</field>
            <field name="model">gestion.establecimiento</field>
            <field name="arch" type="xml">
                <search>
                    <field name="ipress"/>
                    <field name="name"/>
                </search>
            </field>
        </record>

        <!-- VISTA TREE -->
        <record model="ir.ui.view" id="establecimiento_tree_view">
            <field name="name">gestion.establecimiento.tree</field>
            <field name="model">gestion.establecimiento</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="ipress"/>
                    <field name="name"/>
                </tree>
            </field>
        </record>

        <!-- VISTA FORMULARIO -->
        <record model="ir.ui.view" id="establecimiento_form_view">
            <field name="name">gestion.establecimiento.form</field>
            <field name="model">gestion.establecimiento</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group string="Datos del paciente">
                            <group colspan="4">
                                <field name="ipress"/>
                                <field name="name"/>
                            </group>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>


        <!-- ACTION VIEW -->
        <record id="establecimiento_action" model="ir.actions.act_window">
            <field name="name">Establecimientos</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">gestion.establecimiento</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="limit">25</field>
            <field name="domain">[]</field>
            <field name="context">{}</field>
        </record>


        <!-- MENU CITAS -->
        <menuitem id="menu_establecimientos"
            parent="menu_main"
            action="establecimiento_action"
            sequence="5" />



```


[Anterior](./parte-5.md)
